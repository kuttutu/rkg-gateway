// default package
// Generated Jun 22, 2017 12:24:25 AM by Hibernate Tools 3.2.2.GA


import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * ImplUser generated by hbm2java
 */
@Entity
@Table(name="impl_user"
)
public class ImplUser  implements java.io.Serializable {


     private String userId;
     private String name;
     private String emailId;
     private String mobileNo;
     private Set<ImplAddress> implAddresses = new HashSet<ImplAddress>(0);

    public ImplUser() {
    }

	
    public ImplUser(String userId) {
        this.userId = userId;
    }
    public ImplUser(String userId, String name, String emailId, String mobileNo, Set<ImplAddress> implAddresses) {
       this.userId = userId;
       this.name = name;
       this.emailId = emailId;
       this.mobileNo = mobileNo;
       this.implAddresses = implAddresses;
    }
   
     @Id 
    
    @Column(name="user_id", unique=true, nullable=false, length=36)
    public String getUserId() {
        return this.userId;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    @Column(name="name", length=100)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Column(name="email_id", length=200)
    public String getEmailId() {
        return this.emailId;
    }
    
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    
    @Column(name="mobile_no", length=20)
    public String getMobileNo() {
        return this.mobileNo;
    }
    
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="implUser")
    public Set<ImplAddress> getImplAddresses() {
        return this.implAddresses;
    }
    
    public void setImplAddresses(Set<ImplAddress> implAddresses) {
        this.implAddresses = implAddresses;
    }




}


