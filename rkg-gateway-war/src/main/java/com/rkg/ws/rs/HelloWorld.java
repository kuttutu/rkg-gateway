package com.rkg.ws.rs;
import java.math.BigDecimal;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.space.rkg.beans.AddressInfoBean;
import com.space.rkg.beans.OrderInfo;
import com.space.rkg.beans.ProductBean;
import com.vsquare.rkg.pojo.ImplOrderHeader;

@Path("/hello")
public class HelloWorld {

    @GET
    @Path("/echo/{input}")
    @Produces("text/plain")
    public String ping(@PathParam("input") String input) {
        return input;
    }

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/jsonBean")
    public Response modifyJson(JsonBean input) {
        input.setVal2(input.getVal1());
        return Response.ok().entity(input).build();
    }
    
    @GET
    @Path("/requestv1")
    @Produces("application/json")
    public OrderInfo pingOrder() {
    	
    	OrderInfo bean = new OrderInfo();
    	ImplOrderHeader header = new ImplOrderHeader();
    	header.setEmailId("test@gmail.com");
    	header.setMobileNo("8948428989");
    	header.setName("username1");
    	header.setOrderNumber("Order0001");
    	
    	bean.setHeader(header );
    	
    	AddressInfoBean billingAddress = new AddressInfoBean();
    	billingAddress.setAddressLine1("no 5, first street");
    	billingAddress.setAddressLine2("x road");
    	billingAddress.setName("User one");
    	billingAddress.setCity("Coimbatore");
    	billingAddress.setState("TN");
    	billingAddress.setZipCode("54488");
    	bean.setBillingAddress(billingAddress);
    	
    	
    	ProductBean[] items = new ProductBean[3];
    	items[0] = new ProductBean("15a3fb50-5f58-478a-ba95-5c2872f25966","Green Zucchini", new BigDecimal("85.00"), 2, new BigDecimal("170.00"));
    	items[1] = new ProductBean("181751c8-95df-4aa1-b979-8f9be81ed7d7","Romaine lettuce",new BigDecimal("90.00"), 2, new BigDecimal("180.00"));
    	items[2] = new ProductBean("4469213d-8897-4a3a-a345-6e54b4984ba6","Parsley",new BigDecimal("45.00"), 2, new BigDecimal("90.00"));
    	
		bean.setItems(items );
    	
    	return bean;
    }
}

