package com.rkg.ws.rs;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.space.rkg.beans.OrderInfo;
import com.space.rkg.beans.OrderResponse;
import com.vsquare.rkg.pojo.ImplOrderHeader;
import com.vsquare.rkg.services.OrderService;

public class OrderRequestImpl implements OrderRequestSEI{

	public Response saveOrder(OrderInfo order) {
		// TODO Auto-generated method stub
		// validate order info
		OrderService service = new OrderService();
		OrderResponse response = new OrderResponse();
		try {
			ImplOrderHeader header = service.save(order);
			response.setOrderNumber(header.getOrderNumber());
			response.setTransactionId(header.getTransactionId());
			response.setMessage("Order Created Successfully");
			return Response.ok().entity(response).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Issue in creating order").build();
		}
	}

}
