package com.rkg.ws.rs;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.space.rkg.beans.OrderInfo;

@Path("/v1")
public interface OrderRequestSEI {

	@POST
	@Produces("application/json")
    @Consumes("application/json")
	Response saveOrder(OrderInfo order);
}
