package com.space.rkg;
import java.io.File;
import java.net.URL;
import java.util.Set;

import javax.persistence.Entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.reflections.Reflections;

public class SessionManager {

	private static SessionFactory sessionFactory;

	public static Session getSession() throws Exception {
		if(sessionFactory==null){
			setUp();
		}
		return sessionFactory.openSession();
	}

	public static File getCfg() {
		URL url = getResource("hibernate.cfg.xml");
		return url != null ? new File(url.getFile()) : null;
	}
	
	public SessionManager(){
		
	}
	
	public static void loadEntities(MetadataSources metadata){
		Reflections reflections = new Reflections("com.vsquare.rkg.pojo");

		 Set<Class<? extends Object>> allClasses = 
		     reflections.getTypesAnnotatedWith(Entity.class);
		 for(Class c : allClasses){
			 metadata.addAnnotatedClass(c);
		 }
	}
	
	protected static void setUp() throws Exception {

		File cfg = getCfg();
		// A SessionFactory is set up once for an application!
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure(cfg)// configures settings from hibernate.cfg.xml
				.build();
		try {
			MetadataSources metadata = new MetadataSources(registry);
//					.addAnnotatedClassName(
//							"org.hibernate.tutorial.annotations.Event2")
			loadEntities(metadata);
			sessionFactory = metadata.buildMetadata().buildSessionFactory();
		} catch (Exception e) {
			// The registry would be destroyed by the SessionFactory, but we had
			// trouble building the SessionFactory
			// so destroy it manually.
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}

	public static URL getResource(String name) {
//		URL url = SessionManager.class.getResource(name);
		URL url = null;
		if (url == null) {
			url = Thread.currentThread().getContextClassLoader()
					.getResource(name);
		}
		return url;
	}
}
