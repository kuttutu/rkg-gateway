package com.space.rkg.beans;

import java.math.BigDecimal;

public class ProductBean {

	private String productId;
	private String productName;
	private BigDecimal price;
	private Integer quantity;
	private BigDecimal amount;
	
	
	public ProductBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ProductBean(String productId, String productName, BigDecimal price, Integer quantity, BigDecimal amount) {
		this.productId = productId;
		this.price = price;
		this.amount = amount;
		this.productName = productName;
		this.quantity = quantity;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	
}
