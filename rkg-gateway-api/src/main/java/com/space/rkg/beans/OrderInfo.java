package com.space.rkg.beans;

import com.vsquare.rkg.pojo.ImplOrderHeader;

public class OrderInfo {

	private ImplOrderHeader header;
	private AddressInfoBean billingAddress;
	private ProductBean[] items;
	
	public ImplOrderHeader getHeader() {
		return header;
	}
	public void setHeader(ImplOrderHeader header) {
		this.header = header;
	}
	public AddressInfoBean getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(AddressInfoBean billingAddress) {
		this.billingAddress = billingAddress;
	}
	public ProductBean[] getItems() {
		return items;
	}
	public void setItems(ProductBean[] items) {
		this.items = items;
	}
	
	
}
