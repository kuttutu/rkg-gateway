package com.vsquare.rkg.services;

import java.sql.Timestamp;
import java.util.UUID;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.space.rkg.SessionManager;
import com.space.rkg.beans.OrderInfo;
import com.space.rkg.beans.ProductBean;
import com.vsquare.rkg.pojo.ImplAddress;
import com.vsquare.rkg.pojo.ImplOrderHeader;
import com.vsquare.rkg.pojo.ImplOrderInfo;
import com.vsquare.rkg.pojo.ImplProduct;
import com.vsquare.utils.StringUtil;

public class OrderService {

	public ImplOrderHeader save(OrderInfo orderInfo) throws Exception {
		long reqTime = System.currentTimeMillis();
		try(Session hSession = SessionManager.getSession()){
			long sessCreTs = System.currentTimeMillis();
			System.out.println("Session Created in :"+ (sessCreTs - reqTime));
			Transaction tranx = hSession.beginTransaction();
			ImplOrderHeader header = orderInfo.getHeader();
			String transactionId = UUID.randomUUID().toString();
			// set tranx-id
			header.setTransactionId(transactionId);
			if(header.getOrderNumber() == null || "".equals(header.getOrderNumber().trim())){
				header.setOrderNumber(StringUtil.getRandomNumber());
			}
			Timestamp currentTime = new Timestamp(System.currentTimeMillis());
			header.setCreatedTs(currentTime);
			header.setLastModifiedTs(currentTime);
			
			// get items
			ProductBean[] items = orderInfo.getItems();
			for(ProductBean item : items){
				ImplProduct product  = hSession.load(ImplProduct.class, item.getProductId());
				ImplOrderInfo ordInfo = new ImplOrderInfo(UUID.randomUUID().toString(), product, header, item.getPrice(), item.getQuantity(), item.getAmount());
				header.getImplOrderInfos().add(ordInfo);
			}
			
			// get billing address
			ImplAddress billingAddress = new ImplAddress(UUID.randomUUID().toString());
			billingAddress.setAddressLine1(orderInfo.getBillingAddress().getAddressLine1());
			billingAddress.setAddressLine2(orderInfo.getBillingAddress().getAddressLine2());
			billingAddress.setCity(orderInfo.getBillingAddress().getCity());
			billingAddress.setState(orderInfo.getBillingAddress().getState());
			billingAddress.setZipcode(orderInfo.getBillingAddress().getZipCode());
			
			header.getImplAddresses().add(billingAddress);
			
			hSession.save(header);
			tranx.commit();
			long tranxEndTime = System.currentTimeMillis();
			System.out.println("Session tranx completed in : "+ (sessCreTs - tranxEndTime));
			return header;
			
		} catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
