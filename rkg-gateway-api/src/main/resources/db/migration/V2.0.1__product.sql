-- add product
create table impl_product (
	product_id varchar(36) not null primary key,
	product_name varchar(100),
	price numeric(6,2),
	description varchar(500),
	created_ts timestamp,
	last_modified_ts timestamp
);

-- order header
create table impl_order_header (
	transaction_id varchar(36) not null primary key,
	order_number varchar(50) not null,
	name varchar(100),
	email_id varchar(200),
	mobile_no varchar(20),
	created_ts timestamp,
	last_modified_ts timestamp
);

-- user info
create table impl_user (
	user_id varchar(36) primary key,
	name varchar(100),
	email_id varchar(200),
	mobile_no varchar(20)
);

-- address 
create table impl_address (
	address_id varchar(36) primary key,
	address_line_1 varchar(150),
	address_line_2 varchar(150),
	city varchar(50),
	state varchar(50),
	zipcode varchar(10),
	user_id varchar(36) references impl_user
);

-- order info
create table impl_order_info (
	order_info_id varchar(36) primary key,
	transaction_id varchar(36) not null references impl_order_header,
	product_id varchar(36) not null references impl_product,
	price numeric(6,2),
	quantity integer,
	amount numeric(8,2)
);

-- billing info 
create table impl_billing_info (
	billing_id varchar(36) primary key,
	transaction_id varchar(36) references impl_order_header,
	address_id varchar(36) references impl_address
);